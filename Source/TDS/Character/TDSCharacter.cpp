// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "../Game/TDSGameInstance.h"
#include "TDS/FuncLibrary/MyUtils.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	MaxCameraLength = 1200.f;
	MinCameraLength = 300.f;
	ZoomSpeed = 30.f;
	// Create a decal in the world to show the cursor's location
	// CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	// CursorToWorld->SetupAttachment(RootComponent);
	// static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal'"));
	// if (DecalMaterialAsset.Succeeded())
	// {
	// 	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	// }
	// CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	// CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	TirednessMultiplier = TirednessMultiplierMax;
	GetCharacterMovement()->MaxWalkSpeed = SpeedInfo.RunSpeedNormal * TirednessMultiplier;
	SpeedEpsilon = CalculateSpeedEpsilon();

	InitWeapon(InitWeaponName);
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* InComponent)
{
	Super::SetupPlayerInputComponent(InComponent);

	InComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	InComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);
	InComponent->BindAxis(TEXT("MouseWheel"), this, &ATDSCharacter::InputMouseWheel);

	InComponent->BindAction(TEXT("AttackEvent"), IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	InComponent->BindAction(TEXT("AttackEvent"), IE_Released, this, &ATDSCharacter::InputAttackReleased);
	InComponent->BindAction(TEXT("ReloadEvent"), IE_Released, this, &ATDSCharacter::TryReloadWeapon);

}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDSCharacter::InputMouseWheel(float Value)
{
	float NewLength = CameraBoom->TargetArmLength + Value * ZoomSpeed;
	if (NewLength < MinCameraLength)
	{
		NewLength = MinCameraLength;
	}

	if (NewLength > MaxCameraLength)
	{
		NewLength = MaxCameraLength;
	}

	CameraBoom->TargetArmLength = NewLength;
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX, false);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY, false);

	if (MovementState == EMovementState::SprintRun_State)
	{
		FVector myRotationVector = FVector(AxisX,AxisY,0.0f);
		FRotator myRotator = myRotationVector.ToOrientationRotator();
		SetActorRotation((FQuat(myRotator)));
	}
	else
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);// bug was here Config\DefaultEngine.Ini
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

			float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::AimWalk_State:
					CurrentWeapon->ShouldReduceDispersion = true;
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::SprintRun_State:
					break;
				default:
					break;
				}
				
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
				//aim cursor like 3d Widget?
			}
		}
	}
	
	// APlayerController* MyPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	// if (MyPlayerController)
	// {
	// 	FVector MouseLocation, MouseDirection;
	// 	MyPlayerController->DeprojectMousePositionToWorld(MouseLocation, MouseDirection);
	//
	// 	FRotator CurrentCharacterRotation = this->GetActorRotation();
	// 	FRotator TargetRotation = MouseDirection.Rotation();
	//
	// 	FRotator NewRotation = FRotator(CurrentCharacterRotation.Pitch, TargetRotation.Yaw, CurrentCharacterRotation.Roll);
	// 	this->SetActorRotation(NewRotation);
	// }

	GradualApplyTargetSpeed();
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	if (CurrentWeapon)
	{
		//ToDo Check melee or range
		CurrentWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));		
	}
}

void ATDSCharacter::CharacterUpdate()
{
	EMovementState newState = GetMovementState();
	if (newState == MovementState)
	{
		return;
	}

	MovementState = newState;

	float ResSpeed = SpeedInfo.WalkSpeedNormal;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = SpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = SpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = SpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = SpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = SpeedInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}

	UpdateCameraOffset();
	NewTargetSpeed = ResSpeed * TirednessMultiplier;
	CurrentSpeedDelta = (NewTargetSpeed - GetCharacterMovement()->MaxWalkSpeed) / SpeedApplyingStepsAmount;

	AWeaponBase* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDSCharacter::GradualApplyTargetSpeed() const
{
	if (NewTargetSpeed == 0 || GetCharacterMovement()->MaxWalkSpeed == NewTargetSpeed)
	{
		return;
	}

	float Diff = GetCharacterMovement()->MaxWalkSpeed - NewTargetSpeed;
	if (FMath::Abs(Diff) <= SpeedEpsilon)
	{
		GetCharacterMovement()->MaxWalkSpeed = NewTargetSpeed;
		return;
	}

	GetCharacterMovement()->MaxWalkSpeed += CurrentSpeedDelta;
}

EMovementState ATDSCharacter::GetMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		return EMovementState::Run_State;
	}

	if (SprintRunEnabled)
	{
		WalkEnabled = false;
		AimEnabled = false;
		return EMovementState::SprintRun_State;
	}

	if (WalkEnabled && AimEnabled)
	{
		return EMovementState::AimWalk_State;
	}

	if (WalkEnabled && !AimEnabled)
	{
		return EMovementState::Walk_State;
	}

	if (!WalkEnabled && AimEnabled)
	{
		return EMovementState::Aim_State;
	}

	return EMovementState::Walk_State;
}

void ATDSCharacter::TryDecreaseTiredness()
{
	TirednessMultiplier = FMath::Min(TirednessMultiplier + TirednessMultiplierStep, TirednessMultiplierMax);
	CharacterUpdate();
}

void ATDSCharacter::TryIncreaseTiredness()
{
	TirednessMultiplier = FMath::Max(TirednessMultiplier - TirednessMultiplierStep, TirednessMultiplierMin);
	CharacterUpdate();
}

AWeaponBase* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSCharacter::InitWeapon(FName IdWeaponName)
{
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	if (myGI)
	{
		FWeaponInfo MyWeaponInfo;
		if (myGI->TryGetWeaponInfoByName(IdWeaponName, MyWeaponInfo))
		{
			if (MyWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponBase* myWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					CurrentWeapon->Init(MyWeaponInfo, MovementState);
					CurrentWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
					CurrentWeapon->OnWeaponReloadFinish.AddDynamic(this, &ATDSCharacter::WeaponReloadFinish);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

float ATDSCharacter::CalculateSpeedEpsilon() const
{
	const float MaxDiff = SpeedInfo.SprintRunSpeedRun - SpeedInfo.AimSpeedWalk;
	return MaxDiff / SpeedApplyingStepsAmount + 2;
}

void ATDSCharacter::UpdateCameraOffset() const
{
	if (MovementState == EMovementState::Aim_State
		|| MovementState == EMovementState::AimWalk_State)
	{
		if (CurrentCursor == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::UpdateCameraOffset - CurrentCursor -NULL"));
			return;
		}

		FVector CursorPosition = CurrentCursor->GetRelativeLocation() * FVector(1, 1, 0);
		//UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::UpdateCameraOffset - CurrentCursor->GetRelativeLocation() - %s"), *CursorPosition.ToString());
		FVector CameraPosition = CameraBoom->GetComponentToWorld().GetLocation();
		//UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::UpdateCameraOffset - CameraBoom->GetRelativeLocation() - %s"), *CameraPosition.ToString());
		float Distance = FVector::Distance(CameraPosition, CursorPosition);
		//UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::UpdateCameraOffset - Distance - %f"), Distance);
		if (Distance > CameraAimOffsetMinDistance)
		{
			CameraBoom->SetRelativeLocation((FVector(0,0,0) - CursorPosition) * CameraAimOffsetMultiplier);
		}
	}
	else
	{
		CameraBoom->SetRelativeLocation(FVector(0, 0, 0));
		UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::UpdateCameraOffset - OffsetVector to zero"));
	}
}

void ATDSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetCurrentWeaponRound() <= CurrentWeapon->WeaponInfo.MaxRound)
			CurrentWeapon->StartReload();
	}
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDSCharacter::WeaponReloadFinish()
{
	WeaponReloadFinish_BP();
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDSCharacter::WeaponReloadFinish_BP_Implementation()
{
	// in BP
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}
