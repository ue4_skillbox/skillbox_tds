﻿#include "WeaponBase.h"
#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

class AStaticMeshActor;
// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	DropClipTick(DeltaTime);
	DropBulletShellTick(DeltaTime);
}

// Could we simplify that condition?
// We could use the ternary operator to simplify the condition.

void AWeaponBase::FireTick(float DeltaTime)
{
	if (GetCurrentWeaponRound() > 0 && WeaponFiring && FireTime < 0.f && !WeaponReloading)
	{
		Fire();
	}
	else
	{
		FireTime -= DeltaTime;
	}

	if (GetCurrentWeaponRound() <= 0 && !WeaponReloading)
	{
		StartReload();
	}
}

void AWeaponBase::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::DispersionTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		return;
	}

	if (!WeaponFiring)
	{
		CurrentDispersion = ShouldReduceDispersion
		? CurrentDispersion - CurrentDispersionReduction
		: CurrentDispersion + CurrentDispersionReduction;
	}				

	CurrentDispersion = FMath::Clamp(CurrentDispersion, CurrentDispersionMin, CurrentDispersionMax);

	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponBase::DropClipTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.0f)
		{
			DropClipFlag = false;
			InitDropMesh(WeaponInfo.ClipDropMesh);
		}
		else
		{
			DropClipTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::DropBulletShellTick(float DeltaTime)
{
	if (DropBulletShellFlag)
	{
		if (DropBulletShellTimer < 0.0f)
		{
			DropBulletShellFlag = false;
			InitDropMesh(WeaponInfo.ShellBulletsDropMesh);
		}
		else
		{
			DropBulletShellTimer -= DeltaTime;
		}
	}
}

void AWeaponBase::Init(const FWeaponInfo& InitWeaponInfo, const EMovementState MovementState)
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	this->WeaponInfo = InitWeaponInfo;
	this->WeaponCurrentState.Round = InitWeaponInfo.MaxRound;
	UpdateStateWeapon(MovementState);
}

void AWeaponBase::SetWeaponStateFire(bool bIsFire)
{
	if (WeaponFiring == bIsFire)
		return;

	if (CheckWeaponCanFire())
	{
		FireTime = 0.f;
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
	}
}

bool AWeaponBase::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponBase::GetProjectile()
{	
	return WeaponInfo.ProjectileSetting;
}

void AWeaponBase::Fire()
{
	FireTime = WeaponInfo.RateOfFire;
	WeaponCurrentState.Round--;
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponInfo.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponInfo.EffectFireWeapon, ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation(); 

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire
				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix MyMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = MyMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileBase* MyProjectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (MyProjectile)
				{													
					MyProjectile->InitProjectile(WeaponInfo.ProjectileSetting);				
				}
			}
			else
			{
				FHitResult Hit;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponInfo.DistacneTrace,
					ETraceTypeQuery::TraceTypeQuery4, false, Actors, EDrawDebugTrace::ForDuration, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

				if (ShowDebug)
				{
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponInfo.DistacneTrace, FColor::Green, false, 5.f, (uint8)'\000', 0.5f);
				}

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponInfo.ProjectileSetting.HitDecals.Contains(MySurfaceType))
					{
						UMaterialInterface *MyMaterial = WeaponInfo.ProjectileSetting.HitDecals[MySurfaceType];
						if (MyMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(MyMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
						}
					}

					if (WeaponInfo.ProjectileSetting.HitFXs.Contains(MySurfaceType))
					{
						UParticleSystem *MyParticle = WeaponInfo.ProjectileSetting.HitFXs[MySurfaceType];
						if (MyParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MyParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (WeaponInfo.ProjectileSetting.HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponInfo.ProjectileSetting.HitSound, Hit.ImpactPoint);
					}

					UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponInfo.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, nullptr);
				}
			}
		}				
	}
}

void AWeaponBase::UpdateStateWeapon(EMovementState NewMovementState)
{
	if (NewMovementState == EMovementState::SprintRun_State)
	{
		BlockFire = true;
		SetWeaponStateFire(false);
	}
	else
	{
		BlockFire = false;
		std::tuple<float, float, float, float> Dispersion = WeaponInfo.DispersionWeapon.GetDispersionByState(NewMovementState);
		CurrentDispersionMax = std::get<0>(Dispersion);
		CurrentDispersionMin = std::get<1>(Dispersion);
		CurrentDispersionRecoil = std::get<2>(Dispersion);
		CurrentDispersionReduction = std::get<3>(Dispersion);
	}

	// switch (NewMovementState)
	// {
	// case EMovementState::Aim_State:
	// 	
	// 	CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
	// 	CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
	// 	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
	// 	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	// 	break;
	// case EMovementState::AimWalk_State:
	// 	
	// 	CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
	// 	CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
	// 	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
	// 	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	// 	break;
	// case EMovementState::Walk_State:
	// 	
	// 	CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
	// 	CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
	// 	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
	// 	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	// 	break;
	// case EMovementState::Run_State:
	// 	
	// 	CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
	// 	CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
	// 	CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
	// 	CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
	// 	break;
	// case EMovementState::SprintRun_State:
	// 	BlockFire = true;
	// 	SetWeaponStateFire(false);
	// 	break;
	// default:
	// 	break;
	// }
}

void AWeaponBase::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponBase::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponBase::ApplyDispersionToShoot(FVector DirectionShoot) const
{		
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponBase::GetFireEndLocation() const
{
	FVector EndLocation;
	
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if(tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((tmpV).GetSafeNormal()) * -20000.0f;
		if(ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(tmpV), WeaponInfo.DistacneTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion()* PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
			DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Orange, false, 5.f, (uint8)'\000', 2.5f);
		}
	}	
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponInfo.DistacneTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion()* PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
			DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Orange, false, 5.f, (uint8)'\000', 2.5f);
		}
	}
		

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}
		

	return EndLocation;
}

int8 AWeaponBase::GetNumberProjectileByShot() const
{
	return WeaponInfo.NumberProjectileByShot;
}

int32 AWeaponBase::GetCurrentWeaponRound()
{
	return WeaponCurrentState.Round;
}

int32 AWeaponBase::GetCurrentWeaponMaxRound()
{
	return WeaponInfo.MaxRound;
}

float AWeaponBase::GetCurrentWeaponReloadTime()
{
	return WeaponInfo.ReloadTime;
}

void AWeaponBase::StartReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponInfo.ReloadTime;

	WeaponInfo.SoundReloadWeapon ? UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponInfo.SoundReloadWeapon, GetActorLocation()) : nullptr;

	//ToDo Anim reload
	if (WeaponInfo.AnimationWeaponInfo.AnimCharReload)
		OnWeaponReloadStart.Broadcast(WeaponInfo.AnimationWeaponInfo.AnimCharReload);

	if (WeaponInfo.ClipDropMesh.Mesh)
	{
		DropClipFlag = true;
		DropClipTimer = WeaponInfo.ClipDropMesh.Time;
	}
}

void AWeaponBase::FinishReload()
{
	WeaponReloading = false;
	WeaponCurrentState.Round = WeaponInfo.MaxRound;

	OnWeaponReloadFinish.Broadcast();
}

void AWeaponBase::InitDropMesh(FDropMeshInfo MeshInfo)
{
	if (MeshInfo.Mesh)
	{
		FVector LocalDir = this->GetActorForwardVector() * MeshInfo.Offset.GetLocation().X
		+ this->GetActorRightVector() * MeshInfo.Offset.GetLocation().Y
		+ this->GetActorUpVector() * MeshInfo.Offset.GetLocation().Z;

		FTransform Transform;
		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(MeshInfo.Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + MeshInfo.Offset.Rotator()).Quaternion());

		FActorSpawnParameters Params;
		Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Params.Owner = this;

		AStaticMeshActor* NewActor = nullptr;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Params);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			NewActor->SetActorTickEnabled(false);
			NewActor->InitialLifeSpan = MeshInfo.LifeTime;
			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(MeshInfo.Mesh);

			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (MeshInfo.CustomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, MeshInfo.CustomMass, true);
			}

			if (!MeshInfo.ImpulseDirection.IsNearlyZero())
			{
				FVector FinalDirection;
				LocalDir = LocalDir + (MeshInfo.ImpulseDirection * 1000.f);

				if (!FMath::IsNearlyZero(MeshInfo.ImpulseRandomDispersion))
				{
					FinalDirection = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, MeshInfo.ImpulseRandomDispersion);
				}
				FinalDirection.GetSafeNormal(0.000f);
				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDirection * MeshInfo.ImpulsePower);
			}
		}
	}
}
