﻿#include "ProjectileGrenadeBase.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

int32 DebugExplodeShow = 1;
FAutoConsoleVariableRef CVARDebugExplodeShow(
	TEXT("TDS.DebugExplodeShow"),
	DebugExplodeShow,
	TEXT("Show debug explode grenade"),
	ECVF_Cheat);

void AProjectileGrenadeBase::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileGrenadeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DrawDebugSphere(GetWorld(), GetActorLocation(), 10.f, 8, FColor::Red, false, 4.0f);

	TimerExplode(DeltaTime);
}

void AProjectileGrenadeBase::TimerExplode(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplode > TimeToExplode)
		{
			Explode();
		}
		else
		{
			TimerToExplode += DeltaTime;
		}
	}
}

void AProjectileGrenadeBase::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileGrenadeBase::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileGrenadeBase::Explode()
{
	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Green, false, 4.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Red, false, 4.0f);
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExplodeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplodeSound, GetActorLocation());
	}
	
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		1000.0f,
		2000.0f,
		5,
		nullptr, IgnoredActor, nullptr, nullptr);

	this->Destroy();
}