﻿#pragma once

class FMyUtils
{
public:
	static inline void PrintF(const float Value, float TimeToDisplay = 15.0f, FColor DisplayColor = FColor::Yellow);
	static inline void Print(const FString& Value, float TimeToDisplay = 15.0f, FColor DisplayColor = FColor::Yellow);
};

void FMyUtils::PrintF(const float Value, const float TimeToDisplay, const FColor DisplayColor)
{
	const FString TheFloatStr = FString::SanitizeFloat(Value);
	Print(*TheFloatStr, TimeToDisplay, DisplayColor);
}

void FMyUtils::Print(const FString& Value, const float TimeToDisplay, const FColor DisplayColor)
{
	GEngine->AddOnScreenDebugMessage(-1, TimeToDisplay, DisplayColor, *Value);
}